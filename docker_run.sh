sudo docker build . -t analysis_docker

sudo docker run -ti --name analysis_container analysis_docker

sudo docker cp test.c analysis_container:/home/test/test.c
sudo docker cp analysis.sh analysis_container:/home/test/analysis_test.sh

if [ ! "$(sudo docker ps -q -f name=analysis_container)" ]; then
	if [ "$(sudo docker ps -aq -f status=exited -f name=analysis_container)" ]; then
		sudo docker start -ai analysis_container
	fi
fi


## ko smo run ze izvedli
## sudo docker start -ai analysis_container
