gcc test.c 2> gcc_test.txt

valgrind --leak-check=yes ./a.out 2> valgrind_test.txt

cppcheck test.c 2> cppcheck_test.txt
